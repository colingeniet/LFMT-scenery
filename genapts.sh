#!/bin/bash

airports=LFMT

rm -r build/Airport{Area,Obj}

for apt in ${airports}; do
    genapts850 --input=NavData/apt/${apt}.dat --work=build --dem-path=DEM --max-slope=0.1
done
