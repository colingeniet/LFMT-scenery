#!/bin/bash

THREADS=5

rm -r build/DEM
mkdir -p build/DEM

gdalchop build/DEM ${PWD}/data/eu_dem.tiff

terrafit -x 40000 -e 3 build/DEM -j ${THREADS}
