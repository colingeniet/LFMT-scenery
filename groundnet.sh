#!/bin/bash

# uses https://github.com/mherweg/d-laser-fgtools,
# with a couple of custom modifications:
# - add customized airports to parking positions 'move back' exclude list.
# - fix 'tie-down' vs 'tie_down' typo

airports=LFMT

cd groundnet

rm -r Airports
mkdir Airports

for apt in ${airports}; do
    i=$(echo "${apt}" | cut -c 1)
    c=$(echo "${apt}" | cut -c 2)
    a=$(echo "${apt}" | cut -c 3)
    mkdir -p Airports/$i/$c/$a/

    cp ../NavData/apt/${apt}.dat apt.dat
    python "aptdat2sqlite.py"
    python "sqlite2xml.py"
done

cp -r Airports ../
