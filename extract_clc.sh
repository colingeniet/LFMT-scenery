#!/bin/bash

THREADS=5

SHAPEFILES="data/CLC18/"
SHAPEFILES_PREFIX="Code_18_"

function ogr_wrapper {
    ogr-decode --max-segment 200 --line-width 10 --continue-on-errors --threads ${THREADS} $@
}

function extract_mat {
    if [[ -f ${SHAPEFILES}/${SHAPEFILES_PREFIX}$1.shp ]]; then
        echo "extracting material $2 from index $1"
        ogr_wrapper --area-type $2 "build/$2" "${SHAPEFILES}" "${SHAPEFILES_PREFIX}$1" || exit 1
    else
        echo "skipping material $2 from index $1 (no shapefile)"
    fi
}

extract_mat 111 Urban
extract_mat 112 SubUrban
extract_mat 121 Industrial
extract_mat 122 Transport
extract_mat 123 Port
extract_mat 124 Airport
extract_mat 131 OpenMining
extract_mat 132 Dump
extract_mat 133 Construction
extract_mat 141 Greenspace
extract_mat 142 GolfCourse
extract_mat 211 DryCrop
extract_mat 212 IrrCrop
extract_mat 213 Rice
extract_mat 221 Vineyard
extract_mat 222 Orchard
extract_mat 223 Olives
extract_mat 231 MixedCrop
extract_mat 241 MixedCrop
extract_mat 242 ComplexCrop
extract_mat 243 NaturalCrop
extract_mat 244 AgroForest
extract_mat 311 DeciduousForest
extract_mat 312 EvergreenForest
extract_mat 313 MixedForest
extract_mat 321 Grassland
extract_mat 322 Heath
extract_mat 323 Sclerophyllous
extract_mat 324 Scrub
extract_mat 331 Sand
extract_mat 332 Rock
extract_mat 333 HerbTundra
extract_mat 334 Burnt
extract_mat 335 Glacier
extract_mat 411 Marsh
extract_mat 412 Bog
extract_mat 421 SaltMarsh
extract_mat 422 Saline
extract_mat 423 Littoral
extract_mat 511 Watercourse
extract_mat 512 Lake
extract_mat 521 Lagoon
extract_mat 522 Estuary

# Landmass outline, otherwise there are bugs with lakes.
ogr_wrapper --area-type Default build/Default data/clc18_landmass.shp
