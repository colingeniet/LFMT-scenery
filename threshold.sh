#!/bin/bash

airports=LFMT

for apt in ${airports}; do
    dat="NavData/apt/${apt}.dat"

    i=$(echo "${apt}" | cut -c 1)
    c=$(echo "${apt}" | cut -c 2)
    a=$(echo "${apt}" | cut -c 3)
    mkdir -p "Airports/$i/$c/$a/"
    f="Airports/$i/$c/$a/${apt}.threshold.xml"
    rm "${f}"

    {
    echo -ne '<?xml version="1.0" encoding="UTF-8"?>\n\n<PropertyList>\n'
    grep '^100 ' "${dat}" | while read -r line; do
        read prefix width surface shoulder roughness centerlights edgelights distsigns \
            name1 lat1 lon1 disp1 stop1 mark1 aprchlights1 tdz1 reil1 \
            name2 lat2 lon2 disp2 stop2 mark2 aprchlights2 tdz2 reil2 \
            <<< "${line}"

        # calc can't deal with leading 0...
        lat1=$(printf "%.8f" "${lat1}")
        lon1=$(printf "%.8f" "${lon1}")
        lat2=$(printf "%.8f" "${lat2}")
        lon2=$(printf "%.8f" "${lon2}")

        clat1=$(echo "cos(${lat1} /180*pi())" | calc -dp)
        clat2=$(echo "cos(${lat2} /180*pi())" | calc -dp)
        slat1=$(echo "sin(${lat1} /180*pi())" | calc -dp)
        slat2=$(echo "sin(${lat2} /180*pi())" | calc -dp)
        cDlon=$(echo "cos((${lon1} - ${lon2}) /180*pi())" | calc -dp)
        sDlon=$(echo "sin((${lon2} - ${lon1}) /180*pi())" | calc -dp)

        x1=$(echo "${clat2} * ${sDlon}" | calc -dp)
        x2=$(echo "${clat1} * ${sDlon} * -1" | calc -dp)
        y1=$(echo "${clat1}*${slat2} - ${slat1}*${clat2}*${cDlon}" | calc -dp)
        y2=$(echo "${clat2}*${slat1} - ${slat2}*${clat1}*${cDlon}" | calc -dp)

        hdg1=$(echo "mod(atan2(${x1}, ${y1})/pi()*180, 360)" | calc -dp)
        hdg2=$(echo "mod(atan2(${x2}, ${y2})/pi()*180, 360)" | calc -dp)

        echo -ne '  <runway>\n'
        echo -ne '    <threshold>\n'
        printf   '      <rwy>%s</rwy>\n'              ${name1}
        printf   '      <lat>%.8f</lat>\n'            ${lat1}
        printf   '      <lon>%.8f</lon>\n'            ${lon1}
        printf   '      <hdg-deg>%.2f</hdg-deg>\n'    ${hdg1}
        printf   '      <displ-m>%.2f</displ-m>\n'    ${disp1}
        printf   '      <stopw-m>%.2f</stopw-m>\n'    ${stop1}
        echo -ne '    </threshold>\n'
        echo -ne '    <threshold>\n'
        printf   '      <rwy>%s</rwy>\n'              ${name2}
        printf   '      <lat>%.8f</lat>\n'            ${lat2}
        printf   '      <lon>%.8f</lon>\n'            ${lon2}
        printf   '      <hdg-deg>%.2f</hdg-deg>\n'    ${hdg2}
        printf   '      <displ-m>%.2f</displ-m>\n'    ${disp2}
        printf   '      <stopw-m>%.2f</stopw-m>\n'    ${stop2}
        echo -ne '    </threshold>\n'
        echo -ne '  </runway>\n'
    done
    echo -ne '</PropertyList>\n'
    } >> "${f}"
done
