#!/bin/bash

THREADS=5

# Data directories to load
GEN_DIRS="Default DEM AirportArea AirportObj"
CLC_DIRS="Urban SubUrban Industrial Transport Port Airport OpenMining Dump Construction Greenspace GolfCourse DryCrop IrrCrop Rice Vineyard Orchard Olives MixedCrop MixedCrop ComplexCrop NaturalCrop AgroForest DeciduousForest EvergreenForest MixedForest Grassland Heath Sclerophyllous Scrub Sand Rock HerbTundra Burnt Glacier Marsh Bog SaltMarsh Saline Littoral Watercourse Lake Lagoon Estuary Ocean"
# Not loaded: Stream; already loaded from CLC: Watercourse
OSM_DIRS="Road Railroad Canal"

# Arguments: min-lon, max-lon, min-lat, max-lat
function tg () {
    tg-construct --threads=${THREADS} --work-dir=build --output-dir=Terrain --min-lon=$1 --max-lon=$2 --min-lat=$3 --max-lat=$4 \
        ${GEN_DIRS} ${CLC_DIRS} ${OSM_DIRS} # Don't add quotes here!
}

tg 3.93 4 43.55 43.6
