#!/bin/bash

airports=LFMT

for apt in ${airports}; do
    dat="NavData/apt/${apt}.dat"
    grep '^14 ' "${dat}" > /dev/null || continue

    i=$(echo "${apt}" | cut -c 1)
    c=$(echo "${apt}" | cut -c 2)
    a=$(echo "${apt}" | cut -c 3)
    mkdir -p "Airports/$i/$c/$a/"
    f="Airports/$i/$c/$a/${apt}.twr.xml"
    rm "${f}"

    {
    echo -ne '<?xml version="1.0" encoding="UTF-8"?>\n\n<PropertyList>\n  <tower>\n'
    awk -e '/^14 / { printf "    <twr>\n      <lat>%.8f</lat>\n      <lon>%.8f</lon>\n      <elev-m>%.3f</elev-m>\n    </twr>\n", $2, $3, $4 * 0.3048 }' "${dat}"
    echo -ne '  </tower>\n</PropertyList>\n'
    } >> "${f}"
done
