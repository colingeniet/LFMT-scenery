#!/bin/bash

SHAPEFILES="data/OSM_output"
SHAPEFILES_PREFIX="fclass_"

function extract_mat {
    ogr-decode --line-width $3 --area-type $2 "build/$2" "${SHAPEFILES}" "${SHAPEFILES_PREFIX}$1"
}

extract_mat motorway        Road 18
extract_mat motorway_link   Road 9
extract_mat trunk           Road 12
extract_mat trunk_link      Road 8
extract_mat primary         Road 8
extract_mat primary_link    Road 8
extract_mat secondary       Road 7
extract_mat secondary_link  Road 7
extract_mat tertiary        Road 6
extract_mat tertiary_link   Road 6
# Currently unused (not loaded in Terragear.sh)
#extract_mat residential     Road 6
#extract_mat living_street   Road 4
#extract_mat unclassified    Road 6
#extract_mat service         Road 5

extract_mat rail    Railroad 6
extract_mat tram    Railroad 4

extract_mat river   Watercourse 30    # Le Lez (Mosson, Salaison ~10m)
extract_mat stream  Stream      10
extract_mat canal   Canal       20    # canal du Rhône à Sète ~25, canal du bas-Rhône Languedoc ~10
